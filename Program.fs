module fortune_fsharp.App

open System
open System.IO
open System.Net
open System.Diagnostics
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Giraffe.Razor

// ---------------------------------
// Models
// ---------------------------------

type Fortune =
    {
        Message : string
        Number: int
        Hostname: string
        Version: string
    }

// ---------------------------------
// Web app
// ---------------------------------

let random () =
    let generator = Random()
    generator.Next(1000)

let hostname = Dns.GetHostName()

let fortune () =
    let startinfo = ProcessStartInfo()
    startinfo.FileName <- "/bin/sh"
    startinfo.Arguments <- "-c fortune"
    startinfo.RedirectStandardOutput <- true
    startinfo.UseShellExecute <- false
    startinfo.CreateNoWindow <- true
    let proc = new Process()
    proc.StartInfo <- startinfo
    proc.Start() |> ignore
    let result = proc.StandardOutput.ReadToEnd()
    proc.WaitForExit()
    result

// tag::router[]
let fortuneHandler next (ctx: HttpContext) =
    task {
        let obj = { Message = fortune(); Hostname = hostname; Version = "1.2-f#"; Number = random() }
        let header =
            match ctx.GetRequestHeader "Accept" with
                | Error msg -> "text/html"
                | Ok headerValue -> headerValue
        match header with
            | "application/json" ->
                return! ctx.WriteJsonAsync obj
            | "text/plain" ->
                return! ctx.WriteTextAsync (sprintf "Fortune %s cookie of the day #%d:\n\n%s" obj.Version obj.Number obj.Message)
            | _ ->
                return! (razorHtmlView "Index" (Some obj) None None) next ctx
    }

let webApp =
    choose [
        GET >=>
            choose [
                route "/" >=> warbler (fun _ -> fortuneHandler)
            ]
        setStatusCode 404 >=> text "Not Found" ]
// end::router[]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins("http://localhost:8080")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let configureApp (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (match env.IsDevelopment() with
    | true  ->
        app.UseDeveloperExceptionPage()
    | false ->
        app .UseGiraffeErrorHandler(errorHandler)
            .UseHttpsRedirection())
        .UseCors(configureCors)
        .UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    let sp  = services.BuildServiceProvider()
    let env = sp.GetService<IWebHostEnvironment>()
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore
    Path.Combine(env.ContentRootPath, "Views")
    |> services.AddRazorEngine
    |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main args =
    let contentRoot = Directory.GetCurrentDirectory()
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .Configure(Action<IApplicationBuilder> configureApp)
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    .UseUrls("http://0.0.0.0:8080")
                    |> ignore)
        .Build()
        .Run()
    0
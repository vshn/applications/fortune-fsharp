# Stage 1: builder image
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /build
COPY . .
RUN dotnet publish --configuration release --output ./out

# tag::production[]
# Stage 2: runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine
RUN apk add fortune
WORKDIR /dotnetapp
COPY --from=build /build/out .
COPY Views /dotnetapp/Views

EXPOSE 8080

# <1>
USER 1001

ENTRYPOINT ["dotnet", "fortune-fsharp.App.dll"]
# end::production[]
